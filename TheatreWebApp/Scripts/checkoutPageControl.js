﻿var expInitialised = false;
/*var cardDetails = {
    cardNumber: '',
    cardName: '',
    expMonth: '',
    expYear: '',
    cvn: ''
};*/
$(document).ready(function () {

    $("#cardNo").focus(function() {
        jQuery(function($) {
            $("#cardNo").mask("9999-9999-9999-9999", { placeholder: " " });
        });
    });
    
    jQuery(function ($) {
        $("#cvn").mask("999", { placeholder: " " });
    });

    //$('#ddlMonth').click(initializeExpDropdowns);
    //$('#ddlYear').click(initializeExpDropdowns);
    $('#UseExistingDetails').change(togglePaymentFormsVisibility);
    /*getCardDetails();*/
    
});

function removePastMonthsFromDropdown() {
    var thisMonthIndex = new Date().getMonth();
    for (var i = 0; i < thisMonthIndex; i++) {
        $("#ddlMonth option").eq(i).remove().end();
    }
}

function refreshMonths(selectedYear) {
/*    var selectedMonth = parseInt($('#ddlMonth').val());
    var thisMonth = new Date().getMonth + 1;*/
    var thisYear = new Date().getFullYear();
            var selectedMonth = $('#ddlMonth').val();
            var months = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"];
            var option = '';
            var startMonth = 0;
            if (selectedYear == thisYear) {

                startMonth = new Date().getMonth();
            }

            $('#ddlMonth').find('option').remove().end();
        
            for (var i = startMonth; i < months.length; i++) {
                option += '<option value="' + months[i] + '">' + months[i] + '</option>';
            }
            $('#ddlMonth').append(option);
    
            
            
    
            /*if (selectedYear == thisYear) {
        
                removePastMonthsFromDropdown();
            }*/
            
            var monthExists = false;
            $('#ddlMonth option').each(function () {
                if (this.value == selectedMonth) {
                    monthExists = true;
                }
            });
            
            if (monthExists) {
                $('#ddlMonth').val(selectedMonth);
            }
            
            else $('#ddlMonth option:first-child').attr("selected", "selected");
            

    }

function initializeExpDropdowns() {
    if (true) {
        var initialYear = new Date().getFullYear() + 3;
        $("#ddlYear").val(initialYear.toString());
        // removePastMonthsFromDropdown();
        refreshMonths(initialYear.toString());
        expInitialised = true;
    }
}

function setExpDatesToModelValues(month, year) {
    if (parseInt(month) < 10) {
        month = '0' + month;
    }
    $('#ddlMonth').val(month);
    $('#ddlYear').val(year);
}

function togglePaymentFormsVisibility() {
    
    var show = $('#UseExistingDetails');
    if (!show.prop('checked')) {
        $('#expDateDiv').show();
        $('#cvnDiv').show();
        $('#saveDetailsDiv').show();
        $('#cardholdersName').prop('disabled', false);
        $('#cardNo').prop('disabled', false);
        $('#cardType').prop('disabled', false);
        resetFields();
  
    } else {
        //$('#cardholdersName').hide();
        $('#expDateDiv').hide();
        $('#cvnDiv').hide();
        $('#saveDetailsDiv').hide();
        $('#cardholdersName').prop('disabled', true);
        $('#cardNo').prop('disabled', true);
        $('#cardType').prop('disabled', true);
        restoreCardDetails();
    }
}

/*function getCardDetails() {
    cardDetails.cardNumber = $("#cardNumberHidden").val();
    //cardDetails.cardName = $("#cardNameHidden").val();
    cardDetails.cvn = $("#cvnNumberHidden").val();
    cardDetails.expMonth = $('#expMonthHidden').val();
    cardDetails.expYear = $('#expYearHidden').val();
}*/

function restoreCardDetails() {
    $("#cardNo").val($("#cardNumberHidden").val());
    $("#cardholdersName").val($('#cardholdersNameHidden').val());
    $("#cvn").val($("#cvnNumberHidden").val());
    $('#ddlMonth').val($('#expMonthHidden').val());
    $('#ddlYear').val($('#expYearHidden').val());
}

function resetFields() {
    $('#cardholdersName').val('');
    $("#cardNo").val('');
    // $("#cardName").attr('value', '');
    $("#cvn").val('');
    initializeExpDropdowns();
}
