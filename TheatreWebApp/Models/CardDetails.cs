﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Sockets;
using System.Web;

namespace TheatreWebApp.Models
{
    public class CardDetails
    {
        public int Id { get; set; }
        
        public string UserId { get; set; }

        [DisplayName("Card Number")]
        [StringLength(19, MinimumLength = 19, ErrorMessage = "Please enter 16 digit card number")]
        [Required(ErrorMessage = "Please enter credit card number (16 digits)")]
        public string CardNumber { get; set; }

        [DisplayName("Card Type")]
        [Required]
        public string CardType { get; set; }

        [DisplayName("Cardholders Name")]
        [RegularExpression("^[A-Za-z ]+$", ErrorMessage = "Please correct invalid characters in name")]
        [Required]
        public string CardholdersName { get; set; }

        [Required(ErrorMessage = "Please enter CVN number (3 digits)")]
        [DisplayName("CVN Number")]
        [StringLength(3, MinimumLength = 3, ErrorMessage = "Please enter 3 digit CVN number")]
        public string CvnNumber { get; set; }

        public int ExpMonth { get; set; }
        public int ExpYear { get; set; }

        public virtual AspNetUser AspNetUser { get; set; }

    }
}