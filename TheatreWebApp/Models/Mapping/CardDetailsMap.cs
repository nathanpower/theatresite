﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace TheatreWebApp.Models.Mapping
{
    public class CardDetailsMap : EntityTypeConfiguration<CardDetails>
    {
        public CardDetailsMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("CardDetails");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.UserId).HasColumnName("UserId");
            this.Property(t => t.CardNumber).HasColumnName("CardNumber");
            this.Property(t => t.CardType).HasColumnName("CardType");
            this.Property(t => t.ExpMonth).HasColumnName("ExpMonth");
            this.Property(t => t.ExpYear).HasColumnName("ExpYear");
            this.Property(t => t.CvnNumber).HasColumnName("CvnNumber");
            this.Property(t => t.CardholdersName).HasColumnName("CardholdersName");


            // Relationships
            this.HasRequired(t => t.AspNetUser)
                .WithMany(t => t.CardDetails)
                .HasForeignKey(d => d.UserId);

        }
    }
}