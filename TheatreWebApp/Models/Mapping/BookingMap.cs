using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace TheatreWebApp.Models.Mapping
{
    public class BookingMap : EntityTypeConfiguration<Booking>
    {
        public BookingMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.UserId)
                .IsRequired()
                .HasMaxLength(128);

            // Table & Column Mappings
            this.ToTable("Bookings");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.ShowId).HasColumnName("ShowId");
            this.Property(t => t.UserId).HasColumnName("UserId");
            this.Property(t => t.SeatTypeId).HasColumnName("SeatTypeId");
            this.Property(t => t.NumberOfSeats).HasColumnName("NumberOfSeats");
            this.Property(t => t.TotalPrice).HasColumnName("TotalPrice");

            // Relationships
            this.HasRequired(t => t.AspNetUser)
                .WithMany(t => t.Bookings)
                .HasForeignKey(d => d.UserId);
            this.HasRequired(t => t.SeatType)
                .WithMany(t => t.Bookings)
                .HasForeignKey(d => d.SeatTypeId);
            this.HasRequired(t => t.Show)
                .WithMany(t => t.Bookings)
                .HasForeignKey(d => d.ShowId);

        }
    }
}
