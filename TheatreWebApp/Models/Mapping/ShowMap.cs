using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace TheatreWebApp.Models.Mapping
{
    public class ShowMap : EntityTypeConfiguration<Show>
    {
        public ShowMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("Shows");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.ProductionId).HasColumnName("ProductionId");
            this.Property(t => t.DateTime).HasColumnName("DateTime");

            // Relationships
            this.HasRequired(t => t.Production)
                .WithMany(t => t.Shows)
                .HasForeignKey(d => d.ProductionId);

        }
    }
}
