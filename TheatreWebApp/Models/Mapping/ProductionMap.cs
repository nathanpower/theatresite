using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace TheatreWebApp.Models.Mapping
{
    public class ProductionMap : EntityTypeConfiguration<Production>
    {
        public ProductionMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Title)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(50);

            this.Property(t => t.Author)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Productions");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Title).HasColumnName("Title");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.BasePrice).HasColumnName("BasePrice");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Distinction).HasColumnName("Distinction");
            this.Property(t => t.ImageThumbNail).HasColumnName("ImageThumbNail");
            this.Property(t => t.ImageBackground).HasColumnName("ImageBackground");
        }
    }
}
