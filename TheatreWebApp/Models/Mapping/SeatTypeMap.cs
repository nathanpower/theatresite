using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace TheatreWebApp.Models.Mapping
{
    public class SeatTypeMap : EntityTypeConfiguration<SeatType>
    {
        public SeatTypeMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Type)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(10);

            // Table & Column Mappings
            this.ToTable("SeatTypes");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Type).HasColumnName("Type");
            this.Property(t => t.PriceMultiplier).HasColumnName("PriceMultiplier");
            this.Property(t => t.Capacity).HasColumnName("Capacity");
        }
    }
}
