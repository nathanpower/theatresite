using System;
using System.Collections.Generic;
using System.Globalization;

namespace TheatreWebApp.Models
{
    public partial class Show
    {
        public Show()
        {
            this.Bookings = new List<Booking>();
        }

        public int Id { get; set; }
        public int ProductionId { get; set; }
        public System.DateTime DateTime { get; set; }

        public string ViewTime
        {
            get
            {
                /*return this.DateTime.ToString("hh:mm tt", 
                  CultureInfo.InvariantCulture);*/

                return this.DateTime.ToShortTimeString();
            }
        }

        public string ViewDate
        {
            get
            {
                return this.DateTime.ToString("yyyy-MM-dd");
            }
        }

        public virtual ICollection<Booking> Bookings { get; set; }
        public virtual Production Production { get; set; }
    }
}
