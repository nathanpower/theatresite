using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using TheatreWebApp.Models.Mapping;

namespace TheatreWebApp.Models
{
    public partial class TheatreDbContext : DbContext
    {
        static TheatreDbContext()
        {
            System.Data.Entity.Database.SetInitializer<TheatreDbContext>(null);
        }

        public TheatreDbContext()
            : base("Name=TheatreDbContext")
        {
        }

        public DbSet<AspNetRole> AspNetRoles { get; set; }
        public DbSet<AspNetUserClaim> AspNetUserClaims { get; set; }
        public DbSet<AspNetUserLogin> AspNetUserLogins { get; set; }
        public DbSet<AspNetUser> AspNetUsers { get; set; }
        public DbSet<Booking> Bookings { get; set; }
        public DbSet<Production> Productions { get; set; }
        public DbSet<SeatType> SeatTypes { get; set; }
        public DbSet<Show> Shows { get; set; }
        public DbSet<CardDetails> CardDetails { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new AspNetRoleMap());
            modelBuilder.Configurations.Add(new AspNetUserClaimMap());
            modelBuilder.Configurations.Add(new AspNetUserLoginMap());
            modelBuilder.Configurations.Add(new AspNetUserMap());
            modelBuilder.Configurations.Add(new BookingMap());
            modelBuilder.Configurations.Add(new ProductionMap());
            modelBuilder.Configurations.Add(new SeatTypeMap());
            modelBuilder.Configurations.Add(new ShowMap());
            modelBuilder.Configurations.Add(new CardDetailsMap());
        }
    }
}
