﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TheatreWebApp.Models
{
    public class DateDTO
    {
        public int Year { get; set; }
        public int Month { get; set; }
        public int Day { get; set; }

        public string Time { get; set; }
    }
}