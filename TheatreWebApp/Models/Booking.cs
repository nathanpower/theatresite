using System;
using System.Collections.Generic;

namespace TheatreWebApp.Models
{
    public partial class Booking
    {
        public int Id { get; set; }
        public int ShowId { get; set; }
        public string UserId { get; set; }
        public int SeatTypeId { get; set; }
        public int NumberOfSeats { get; set; }
        public double TotalPrice { get; set; }
        public virtual AspNetUser AspNetUser { get; set; }
        public virtual SeatType SeatType { get; set; }
        public virtual Show Show { get; set; }
    }
}
