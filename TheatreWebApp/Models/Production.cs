using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using TheatreWebApp.Repository;

namespace TheatreWebApp.Models
{
    public partial class Production
    {
        public Production()
        {
            this.Shows = new List<Show>();
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }
        public double BasePrice { get; set; }

        public string Description { get; set; }
        public string Distinction { get; set; }
        public string ImageThumbNail { get; set; }
        public string ImageBackground { get; set; }
        public virtual ICollection<Show> Shows { get; set; }

        [NotMapped]
        public string DateRange { get; set; }
    }
}
