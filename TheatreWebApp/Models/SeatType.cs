using System;
using System.Collections.Generic;

namespace TheatreWebApp.Models
{
    public partial class SeatType
    {
        public SeatType()
        {
            this.Bookings = new List<Booking>();
        }

        public int Id { get; set; }
        public string Type { get; set; }
        public double PriceMultiplier { get; set; }
        public int Capacity { get; set; }
        public virtual ICollection<Booking> Bookings { get; set; }
    }
}
