﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.Threading;
using System.Web;
using System.Web.UI.WebControls;
using Microsoft.Ajax.Utilities;
using TheatreWebApp.Models;

namespace TheatreWebApp.Repository
{
    public class TheatreRepository : ITheatreRepository
    {
        readonly TheatreDbContext _db = new TheatreDbContext();
        public IEnumerable<Show> GetShows()
        {
            return _db.Shows;
        }

        public IEnumerable<Production> GetProductions()
        {
            var allProductions = _db.Productions;
            return allProductions.Where(production => production.Shows.Any(show => show.DateTime > DateTime.Now)).ToList();
        }

        public IEnumerable<SeatType> GetSeatTypes()
        {
            return _db.SeatTypes;
        }

        public IEnumerable<Show> GetShowsForDate(DateTime date)
        {
            try
            {
           
                return
                    _db
                    .Shows
                    .Where(x => x.DateTime.Year == date.Year
                            && x.DateTime.Month == date.Month
                            && x.DateTime.Day == date.Day).ToList();
                }
            catch (Exception e)
            {
                return null;
            }
        }

        public IEnumerable<string> GetDatesForProductions(int productionId)
        {
            List<Show> shows = productionId != 0 ? 
                _db.Shows.Where(x => x.ProductionId == productionId).OrderBy(y => y.DateTime).ToList() 
                : _db.Shows.OrderBy(s => s.DateTime).ToList();

            string[] result = new string[shows.Count];
           // Thread.Sleep(1000);
            for (int i = 0; i < shows.Count; i++)
            {
                result[i] = shows[i].ViewDate;
            }

            return result;
        }


        public IEnumerable<string> GetLongDatesForProductions(int productionId)
        {
            List<Show> shows = productionId != 0 ?
                _db.Shows.Where(x => x.ProductionId == productionId && x.DateTime >= DateTime.Now).OrderBy(y => y.DateTime).ToList()
                : _db.Shows.Where(x => x.DateTime >= DateTime.Now).OrderBy(s => s.DateTime).ToList();

            string[] result = new string[shows.Count];
            // Thread.Sleep(1000);
            for (int i = 0; i < shows.Count; i++)
            {
                result[i] = shows[i].DateTime.ToLongDateString();
            }

            return result;
        }

        public List<Production> GetProductionsForDate(DateTime date)
        {
            var shows = GetShowsForDate(date);
            List<Production> result = new List<Production>();
            try
            {
                foreach (var show in shows)
                {
                    result = _db.Productions.Where(p => p.Id == show.ProductionId).ToList();
                }
            }
            catch (Exception e)
            {
                return result;
            }
            return result;
        }

        public IEnumerable<Show> GetShowsForProduction(DateTime dateTime, int productionId)
        {
            return _db.Shows.Where(s => s.ProductionId == productionId &&
                                        s.DateTime.Day == dateTime.Day &&
                                        s.DateTime.Month == dateTime.Month &&
                                        s.DateTime.Year == dateTime.Year).ToList();
        }

        public IList<CardType> GetCardTypes()
        {
            var cardTypes = new List<CardType>();

            cardTypes.Add(new CardType()
            {
                Id = 0,
                Company = "Mastercard"
            });

             cardTypes.Add(new CardType()
            {
                Id = 1,
                Company = "Visa"
            });
            return cardTypes;
        }

        public IList<string> GetMonthsList()
        {
            var months = new [] { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"};
            return months;
        }

        public IList<string> GetYearsList()
        {
            var now = DateTime.Now;
            var years = new string[10];
            for (int i = 0; i < years.Length; i++)
            {
                years[i] = now.AddYears(i).Year.ToString(CultureInfo.InvariantCulture);
            }
            return years;
        }

        public string GetDateRangeForProduction(int id)
        {
            var dates = GetLongDatesForProductions(id);
            var first = dates.First();
            var last = dates.Last();
            return first + " - " + last;
        }

        public IEnumerable<Production> GetDateRangesForProductions(IEnumerable<Production> productions)
        {

            foreach (var production in productions)
            {
                production.DateRange = GetDateRangeForProduction(production.Id);
            }
            return productions;
        }

        public IEnumerable<Show> GetUpcomingShows()
        {
            var allUpcomingShows =
                _db.Shows.Where(s => s.DateTime >= DateTime.Now).Distinct().OrderBy(d => d.DateTime).ToList();
            return allUpcomingShows.DistinctBy(s => s.ProductionId).Take(3);
        }

        public IEnumerable<Production> GetProductionsNotShownInUpcomingShows(IEnumerable<Show> shows)
        {
            var ids = (from show in shows
                select show.ProductionId);
            return _db.Productions.Where(p => !ids.Contains(p.Id));
        }
    }

}