﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheatreWebApp.Models;

namespace TheatreWebApp.Repository
{
    public interface ITheatreRepository
    {
        IEnumerable<Show> GetShows();
        IEnumerable<Production> GetProductions();
        IEnumerable<SeatType> GetSeatTypes();
    }
}
