﻿



$(document).ready(function () {
    $("#prodRadio").click(enableTicketByProduction);
    
    $("#dateRadio").click(enableTicketByDate);
    
    $('#numbertickets').spinner({
        required: true,
        increment: 1,
        min: 1,
        max: 4
    }).val("2");

    enableTicketByProduction();
});

function enableTicketByProduction() {
    $("#ddlMainProd").val(-1);
    $("#ticketByDate :input").attr("disabled", true);
    $("#ticketByDate").children().hide();
    $("#ticketByProduction").children().show();
    $("#ticketByProduction :input").attr("disabled", false);
    $("#dateRadio").attr("disabled", false);
    $("#ddlDateCBP").parent().hide();
    $("#ddlShow2").hide();
    $("#ddlSeatType").val(-1);
    $("#ddlShow").attr("name", "");
    $("#ddlShow2").attr("name", "SelectedShowId");
    $("#ddlProductions").attr("name", "");
    $("#ddlMainProd").attr("name", "SelectedProductionId");
    
}

function enableTicketByDate() {
    $("#ddlDateCBD").val(-1);
    GetDates(0, $("#ddlDateCBD"), null);
    $("#ticketByProduction :input").attr("disabled", true);
    $("#ticketByDate").children().show();
    $("#ticketByProduction").children().hide();
    $("#ticketByDate :input").attr("disabled", false);
    $("#prodRadio").attr("disabled", false);
    $("#ddlProductions").hide();
    $("#ddlShow").hide();
    $("#ddlSeatType").val(-1);
    $("#ddlShow").attr("name", "SelectedShowId");
    $("#ddlShow2").attr("name", "");
    $("#ddlMainProd").attr("name", "");
    $("#ddlProductions").attr("name", "SelectedProductionId");
    
}

function GetProductions(date, resetTarget) {

    if (resetTarget) {
        resetTarget.find('option').remove().end();
        resetTarget.show();
        $("#ddlShow").hide();
    }

    var processmessage = "<option value='0'> Please wait...</option>";
    resetTarget.html(processmessage).show();
    var url = "/Booking/GetProductionsByDate/";

    $.ajax({
        url: url,
        data: { date: date },
        cache: false,
        type: "POST",
        success: function (data) {
            
            var markup = "<option value='0'>Select Production...</option>";
            for (var x = 0; x < data.length; x++) {
                markup += "<option value=" + data[x].Value + ">" + data[x].Text + "</option>";
            }
            resetTarget.html(markup).show();
        },
        error: function (reponse) {
            alert("error : " + reponse);
        }
    });

}

function GetShowsForProduction(productionId, picker, target) {
    var date = picker.val();
    var processmessage = "<option value='0'> Please wait...</option>";
    target.html(processmessage).show();
    var url = "/Booking/GetShowsForProduction/";

    $.ajax({
        url: url,
        data: {
            date: date,
            productionId: productionId
        },
        cache: false,
        type: "POST",
        success: function (data) {
            target.show();
            var markup = "<option value='0'>Select Show...</option>";
            for (var x = 0; x < data.length; x++) {
                markup += "<option value=" + data[x].Value + ">" + data[x].Text + "</option>";
            }
            target.html(markup).show();
        },
        error: function(reponse) {
            alert("error : " + reponse);
        }
    });
}

function GetDates(productionId, picker, resetTarget) {

    if (productionId >= 0) {
        if (resetTarget) {
            resetTarget.find('option').remove().end();
            picker.parent().show();
            picker.show();
            resetTarget.hide();
        }
        var processmessage = "Please wait...";
        picker.val(processmessage).show();
        var url = "/Booking/GetDatesForProduction/";

        $.ajax({
            url: url,
            data: {
                productionId: productionId,
            },
            cache: false,
            type: "POST",
            success: function(data) {
                picker.datepicker("destroy");
                EnableOnlyValidDaysOnCalendar(data, picker);
                picker.focus();
            },
            error: function(reponse) {
                alert("error : " + reponse);
            }
        });
    } else (picker.val(''));

}

function EnableOnlyValidDaysOnCalendar(datesAllowed, picker) {

    var earliestShowingDate = splitDate(datesAllowed[0]);
    var defaultYear = parseInt(earliestShowingDate[0]);
    var defaultMonth = parseInt(earliestShowingDate[1]);
    var defaultDay = parseInt(earliestShowingDate[2]);
    var defaultDate = new Date(defaultYear, defaultMonth - 1, defaultDay + 1);
    var viewDate = getShortUTCDateString(defaultDate.toUTCString());
    var today = new Date();
    //defaultDate.setFullYear(defaultYear, defaultMonth, defaultDay);
    picker.val(viewDate);


    picker.datepicker({
        // these aren't necessary, but if you happen to know them, why not
        minDate: today,
        maxDate: new Date(today.getFullYear + 1, today.getMonth + 7, today.getDay),
        //dateFormat: 'yyyy-mm-dd',
        dateFormat: $.datepicker.RFC_1123,
         defaultDate: defaultDate,
        //defaultDate: $.datepicker.parseDate("y m d", datesAllowed[0].replace("-"," ")),
        // called for every date before it is displayed
        beforeShowDay: function(date) {

            // prepend values lower than 10 with 0
            function addZero(no) {
                if (no < 10){
                    return "0" + no;
                }  else {
                    return no; 
                }
            }

            var dateStr = [
                addZero(date.getFullYear()),
                addZero(date.getMonth() + 1),
                addZero(date.getDate())      
            ].join('-');

            if ($.inArray(dateStr, datesAllowed) > -1) {
                return [true, 'good_date', 'This date is selectable'];
            } else {
                return [false, 'bad_date', 'This date is NOT selectable'];
            }
        }
    });


}

function splitDate(date) {
    return date.split("-");
}

function getShortUTCDateString(date) {
    return date.split(" ").slice(0, 4).join(" ");
}


