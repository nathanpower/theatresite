﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TheatreWebApp.Startup))]
namespace TheatreWebApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
