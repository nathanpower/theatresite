﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TheatreWebApp.Models;

namespace TheatreWebApp.ViewModels
{
    public class HomePageViewModel
    {
        public IEnumerable<Show> UpcomingShows { get; set; }
        public IEnumerable<Production> Productions { get; set; }

    }
}