﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TheatreWebApp.Repository;

namespace TheatreWebApp.ViewModels
{
    public class CalendarEventViewModel
    {
        public int id { get; set; }

        public string title { get; set; }
        public DateTime start { get; set; }
        public bool allDay { get; set; }

        public string url { get; set; }
    }
}