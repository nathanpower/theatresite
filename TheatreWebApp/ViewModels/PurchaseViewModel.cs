﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TheatreWebApp.Models;
using TheatreWebApp.Repository;

namespace TheatreWebApp.ViewModels
{
    public class PurchaseViewModel
    {
        private ITheatreRepository _repository;

        public PurchaseViewModel()
        {
            _repository = new TheatreRepository();
        }

        public PurchaseViewModel(ITheatreRepository repository)
        {
            _repository = repository;
        }


        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Please select a showing")]
        public int SelectedShowId { get; set; }

        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Please select a seat type")]
        public int SelectedSeatTypeId { get; set; }
        
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Please select a production")]
        public int SelectedProductionId { get; set; }
        [Required]
        public string SelectedTime { get; set; }
        [Required]
        public DateTime SelectedDate { get; set; }

        public IEnumerable<SelectListItem> Shows
        {
            get
            {
                var shows = _repository.GetShows().Select(s => new SelectListItem
                {
                    Value = s.Id.ToString(CultureInfo.InvariantCulture),
                    Text = s.DateTime.ToLongDateString() + " " + s.DateTime.ToShortTimeString()
                });
                return DefaultShowItem.Concat(shows); 
            }
        }

        public IEnumerable<SelectListItem> Productions
        {
            get
            {
                var productions = _repository.GetProductions().Select(s => new SelectListItem
                {
                    Value = s.Id.ToString(CultureInfo.InvariantCulture),
                    Text = s.Title
                });
                return DefaultProductionItem.Concat(productions); 
            }
        }

        public IEnumerable<SelectListItem> SeatTypes
        {
            get
            {
                var seatTypes = _repository.GetSeatTypes().Select(s => new SelectListItem
                {
                    Value = s.Id.ToString(CultureInfo.InvariantCulture),
                    Text = s.Type
                });
                return DefaultSeatTypeItem.Concat(seatTypes); 
            }
        }

        public Show Show { get; set; }

        public SeatType Seat { get; set; }

        public int NumberOfTickets { get; set; }

        public IEnumerable<SelectListItem> DefaultSeatTypeItem
        {
            get
            {
                return Enumerable.Repeat(new SelectListItem
                {
                    Value = "-1",
                    Text = "Select Seat Type..."
                }, count: 1);
            }
        }

        public IEnumerable<SelectListItem> DefaultProductionItem
        {
            get
            {
                return Enumerable.Repeat(new SelectListItem
                {
                    Value = "-1",
                    Text = "Select Production..."
                }, count: 1);
            }
        }

        public IEnumerable<SelectListItem> DefaultShowItem
        {
            get
            {
                return Enumerable.Repeat(new SelectListItem
                {
                    Value = "-1",
                    Text = "Select Show..."
                }, count: 1);
            }
        }
    }
}