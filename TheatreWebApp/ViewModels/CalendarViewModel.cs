﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using TheatreWebApp.Repository;

namespace TheatreWebApp.ViewModels
{
    public class CalendarViewModel
    {
        public CalendarViewModel(ITheatreRepository repository)
        {
            var shows = repository.GetShows();
            var _events = shows.Select(show => new CalendarEventViewModel
            {
                id = show.Id, title = show.Production.Title, start = show.DateTime, allDay = false, 
                url = "/productions/details/" + show.ProductionId + 
                "?date="+show.DateTime.ToString("g")
            }).ToList();

            events = _events;
            editable = false;
        }
        public bool editable { get; set; }
        public IEnumerable<CalendarEventViewModel> events { get; set; }
    }
}