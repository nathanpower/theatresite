﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TheatreWebApp.Models;

namespace TheatreWebApp.ViewModels
{
    public class BookingViewModel
    {


        TheatreDbContext db = new TheatreDbContext();

        public int NumberOfSeats { get; set; }

        public int SeatType { get; set; }
        public DateTime DateTime { get; set; }

        public Show Show { get; set; }

        public SelectList SeatTypes
        {
            get
            {
                return new SelectList(db.SeatTypes, "Id", "Type");
            }

        }

        public SelectList Productions
        {
            get
            {
                return new SelectList(db.Productions, "Id", "Title");
            }
        }

        public IEnumerable<Show> Shows
        {
            get
            {
                return db.Shows;
            }
        }

    }
}