﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.Odbc;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Web;
using System.Web.Mvc;
using TheatreWebApp.Models;
using TheatreWebApp.Repository;

namespace TheatreWebApp.ViewModels
{
    public class CheckoutViewModel
    {
        TheatreRepository _repository = new TheatreRepository();
        public Show Show { get; set; }
        public Production Production { get; set; }
        public int NumberOfTickets { get; set; }
        public SeatType SeatType { get; set; }
        public CardDetails CardDetails { get; set; }

        [Required(ErrorMessage = "Please enter your email address")]
        [DisplayName("Email Address")]       
        public string EmailAddress { get; set; }

        [Required(ErrorMessage = "Please enter your mobile number")]
        [DisplayName("Mobile Number")]
        public string PhoneNumber { get; set; }

        public bool UserIsLoggedIn { get; set; }

        [DisplayName("Save Details")]
        public bool SaveDetails { get; set; }

        [DisplayName("Use Existing Details")]
        public bool UseExistingDetails { get; set; }

        public string TicketPrice
        {
            get
            {
                return String.Format("€{0:0.00}", (Production.BasePrice * SeatType.PriceMultiplier));
            }
        }

        public string TotalPrice
        {
            get
            {
                return String.Format("€{0:0.00}", (Production.BasePrice * SeatType.PriceMultiplier * NumberOfTickets));
            }
        }

        public IEnumerable<SelectListItem> CardTypes
        {
            get
            {
                var repository = new TheatreRepository();
                IList<CardType> cardTypes = _repository.GetCardTypes();
                IEnumerable <SelectListItem> selectList =
                from c in cardTypes
                select new SelectListItem
                {
                    Text = c.Company,
                    Value = c.Id.ToString()
                };
                return selectList;
            }
        }

        public IEnumerable<SelectListItem> Months
        {
            get
            {
                IList<string> monthsList = _repository.GetMonthsList();
                IEnumerable<SelectListItem> selectList =
                from c in monthsList
                select new SelectListItem
                {
                    Text = c,
                    Value = c
                };
                return selectList;
            }
        }

        public IEnumerable<SelectListItem> Years
        {
            get
            {
                IList<string> yearsList = _repository.GetYearsList();
                IEnumerable<SelectListItem> selectList =
                from c in yearsList
                select new SelectListItem
                {
                    Text = c,
                    Value = c
                };
                return selectList;
            }
        }
    }
}