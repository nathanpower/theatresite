﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using TheatreWebApp.Models;
using TheatreWebApp.Repository;
using TheatreWebApp.ViewModels;

namespace TheatreWebApp.Controllers
{
    public class BookingController : Controller
    {

        //
        // GET: /Booking/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Booking/Create
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Booking/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index", "Home");
            }
            catch
            {
                return View();
            }
        }

        // GET: /Booking/Calendar
        public ActionResult Calendar()
        {
            var eventsJson = JsonConvert.SerializeObject(new CalendarViewModel(new TheatreRepository()));
            ViewBag.Events = eventsJson;
            return View();
        }


        //[Authorize]
        public ActionResult Purchase(int? id, string date)
        {
            var model = new PurchaseViewModel(new TheatreRepository());
            if(id != null && id > 0) model.SelectedProductionId = (int) id;
            if (date != null)
            {
                var selectedDate = DateTime.Parse(date);
                ViewBag.Date = new DateDTO()
                {
                    Year = selectedDate.Year,
                    Month = selectedDate.Month,
                    Day = selectedDate.Day,
                    Time = selectedDate.ToShortTimeString()
                };
            }

            return View(model);
        }



        //Action result for ajax call
        [HttpPost]
        public ActionResult GetProductionsByDate(string date)
        {
            var dateTime = Convert.ToDateTime(date);
            var repository = new TheatreRepository();
            var productions = repository.GetProductionsForDate(dateTime);
            var prodSelectList = new SelectList(productions, "Id", "Title", 0);
            return Json(prodSelectList);
        }

        //Action result for ajax call
        [HttpPost]
        public ActionResult GetShowsForProduction(string date, string productionid)
        {
            var dateTime = Convert.ToDateTime(date);
            var prodId = Int32.Parse(productionid);
            var repository = new TheatreRepository();
            var shows = repository.GetShowsForProduction(dateTime, prodId);
            var prodSelectList = new SelectList(shows, "Id", "ViewTime", 0);
            return Json(prodSelectList);
        }

        //Action result for ajax call
        [HttpPost]
        public ActionResult GetDatesForProduction(string productionid)
        {
            var prodId = Int32.Parse(productionid);
            var repository = new TheatreRepository();
            var dates = repository.GetDatesForProductions(prodId);
            return Json(dates);
        }

        public ActionResult Checkout()
        {
            return RedirectToAction("Purchase");
        }

        //[Authorize]
        [HttpPost]
        public ActionResult Checkout(PurchaseViewModel purchaseViewModel)
        {
            var db = new TheatreDbContext();
            CardDetails existingCardDetails = null;
            var userIsLoggedIn = User.Identity.IsAuthenticated;

            if (userIsLoggedIn)
            {
                var username = User.Identity.Name;
                var userId = db.AspNetUsers.SingleOrDefault(x => x.UserName == username).Id;
                existingCardDetails = db.CardDetails.SingleOrDefault(x => x.UserId == userId);
            }

            var production = db.Productions.Single(p => p.Id == purchaseViewModel.SelectedProductionId);
            var show = db.Shows.Single(s => s.Id == purchaseViewModel.SelectedShowId);
            var seatType = db.SeatTypes.Single(s => s.Id == purchaseViewModel.SelectedSeatTypeId);
            var model = new CheckoutViewModel()
            {
                NumberOfTickets = purchaseViewModel.NumberOfTickets,
                Production = production,
                Show = show,
                SeatType = seatType,
                CardDetails = new CardDetails(),
                UserIsLoggedIn = userIsLoggedIn
            };

            if (existingCardDetails != null)
            {
                model.CardDetails = existingCardDetails;
                model.UseExistingDetails = true;
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult ConfirmOrder(CheckoutViewModel model)
        {
            var db = new TheatreDbContext();
            CardDetails existingCardDetails = null;
            string userId = null;
            var userIsLoggedIn = User.Identity.IsAuthenticated;

            if (userIsLoggedIn)
            {
                var username = User.Identity.Name;
                userId = db.AspNetUsers.SingleOrDefault(x => x.UserName == username).Id;
                existingCardDetails = db.CardDetails.SingleOrDefault(x => x.UserId == userId);
            }
            
            if (model.SaveDetails && !userId.IsNullOrEmpty())
            {
                model.CardDetails.CardNumber = ObscureCardNumber(model.CardDetails.CardNumber);

                var cardDetails = new CardDetails()
                {
                    CardNumber = model.CardDetails.CardNumber,
                    CardholdersName = model.CardDetails.CardholdersName,
                    CvnNumber = model.CardDetails.CvnNumber,
                    CardType = model.CardDetails.CardType,
                    ExpMonth = model.CardDetails.ExpMonth,
                    ExpYear = model.CardDetails.ExpYear,
                    UserId = userId
                };
                if (existingCardDetails == null)
                {
                    db.CardDetails.Add(cardDetails);
                    db.SaveChanges();
                }
                else
                {
                    existingCardDetails.CardNumber = model.CardDetails.CardNumber;
                    existingCardDetails.CardholdersName = model.CardDetails.CardholdersName;
                    existingCardDetails.CardType = model.CardDetails.CardType;
                    existingCardDetails.CvnNumber = model.CardDetails.CvnNumber;
                    existingCardDetails.ExpMonth = model.CardDetails.ExpMonth;
                    existingCardDetails.ExpYear = model.CardDetails.ExpYear;
                    db.SaveChanges();
                }
            }
            return View(model);
        }

        private string ObscureCardNumber(string input)
        {
            string result = "****-****-****-";
            var charArray = input.ToCharArray();
            for (int i = 15; i < 19; i++)
            {
                result += charArray.GetValue(i);
            }
            
            return result;
        }
     
    }
}
