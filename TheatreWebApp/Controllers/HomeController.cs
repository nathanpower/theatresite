﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TheatreWebApp.Repository;
using TheatreWebApp.ViewModels;

namespace TheatreWebApp.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var repository = new TheatreRepository();
            var upcomingShows = repository.GetUpcomingShows().ToList();
            var notShownProductions = 
                repository.GetProductionsNotShownInUpcomingShows(upcomingShows).ToList().Take(3);
            notShownProductions = repository.GetDateRangesForProductions(notShownProductions);

            var model = new HomePageViewModel()
            {
                UpcomingShows = upcomingShows,
                Productions = notShownProductions
            };
            return View(model);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}