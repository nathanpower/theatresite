﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TheatreWebApp.Models;
using TheatreWebApp.Repository;

namespace TheatreWebApp.Controllers
{
    public class ProductionsController : Controller
    {
        //
        // GET: /Productions/
      /*  public ActionResult Details(int id)
        {
            var db = new TheatreDbContext();
            var model = db.Productions.SingleOrDefault(x => x.Id == id);
            model.DateRange = new TheatreRepository().GetDateRangeForProduction(id);
            return View(model);
        }
*/
        public ActionResult Details(int id, string date)
        {
            var db = new TheatreDbContext();
            var model = db.Productions.SingleOrDefault(x => x.Id == id);
            model.DateRange = new TheatreRepository().GetDateRangeForProduction(id);
            if (date != null)
            {
                var selectedDate = DateTime.Parse(date);
                ViewBag.Date = selectedDate;
            }
            return View(model);
        }
	}
}