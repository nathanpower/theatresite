﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TheatreWebApp.Models;
using TheatreWebApp.Repository;

namespace TheatreWebApp.UnitTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void GetShowsForProductionAlwaysReturnsShows()
        {
            var repository = new TheatreRepository();
            DateTime date = DateTime.Parse("2014-05-01 20:00:00");
            var result = repository.GetShowsForProduction(date, 1);
            Assert.IsTrue(result.Any());
        }
    }
}
